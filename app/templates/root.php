<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title><?php echo $titulo; ?></title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/bootstrap-theme.min.css">
<link rel="stylesheet" href="../css/main.css">
<script src="../js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
	<script type="text/javascript" src="js/highcharts.js"></script>
	<script type="text/javascript" src="js/highcharts-3d.js"></script>

	<script type="text/javascript" src="js/exporting.js"></script>
</head>
<body onload="Aplicacion.main();">
  <!--[if lt IE 7]>
  <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->
<div class="row">
	<div class="col-md-2">
		<img src="/img/info_financiera_top1.jpg" class="img-thumbnail" alt="Responsive image">
	</div>
	<div class="col-md-8">
		<h3><strong><p class="text-center"><?php echo $titulo; ?></p></strong></h3>
	</div>
	<div class="col-md-2">
		<img src="/img/info_financiera_top2.jpg" class="img-thumbnail" alt="Responsive image">
	</div>	
</div>

<div class="navbar navbar-default" role="navigation">
	<div class="col-md-6 col-md-offset-6">.
		<div class="container-fluid">
			<div class="navbar-header">
		    	<a class="navbar-brand" href="/"><span class="glyphicon glyphicon-home"></span> Inicio</a>
		  	</div>
			<div class="navbar-header">
		  	  <a class="navbar-brand" href="../about/">Acerca de</a>
		  	</div>
			<div class="navbar-header">
		    	<a class="navbar-brand" href="/api/">API</a>
		  	</div>
		  	<div class="navbar-header">
		  	  <a class="navbar-brand" href="../../docs/">Documentación</a>
		  	</div>
		</div>
	</div>
</div>

<hr>

<!--<script src="js/codigo.js"></script>-->
<script src="js/codigo.js"></script>


<hr/>
<div class="container-fluid">
</div>

 
  <script src="../js/vendor/jquery-1.11.0.min.js"></script>
  <script src="../js/vendor/bootstrap.min.js"></script>
  <script src="../js/main.js"></script>

  </body>
</html>
